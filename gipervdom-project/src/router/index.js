import Vue from 'vue'
import Router from 'vue-router'
import indexPage from '@/components/IndexPage'
import contactsPage from '@/components/content/ContactsPage'
import howToOffer from '@/components/content/HowToOffer'
import delivery from '@/components/content/Delivery'
import reviews from '@/components/content/Reviews'
import catalogIndex from '@/components/catalog/CatalogIndex'
import catalogSection from '@/components/catalog/CatalogSection'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: indexPage
    },
    {
      path: '/how-to/',
      name: 'how-to',
      component: howToOffer
    },
    {
      path: '/reviews/',
      name: 'reviews',
      component: reviews
    },
    {
      path: '/contacts/',
      name: 'contacts',
      component: contactsPage
    },
    {
      path: '/about/',
      name: 'about',
      component: indexPage
    },
    {
      path: '/delivery/',
      name: 'delivery',
      component: delivery
    },
    {
      path: '/catalog/',
      name: 'catalog',
      component: catalogIndex
    },
    {
      path: '/catalog/:slug',
      name: 'catalogSection',
      component: catalogSection,
      props: true
    },
    {
      path: '/personal/',
      name: 'personal',
      component: indexPage
    },
    {
      path: '/basket/',
      name: 'basket',
      component: indexPage
    }
  ]
})
