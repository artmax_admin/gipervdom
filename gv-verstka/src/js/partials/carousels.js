jQuery(document).ready(function ($) {
    // слайдер баннеров на главной
    $('.slide-carousel.owl-carousel').owlCarousel({
        items: 1
    });

    // слайдер товаров в обзоре категорий
    $('.category__overview--positions').owlCarousel({
        items: 3,
        nav: true,
        navText: ['', ''],
        responsive: {
            1224: {
                items: 4
            }
        }
    });

    // слайдер товаров в согласовании заказа
    $('.item__suggestions').owlCarousel({
        items: 3,
        margin: 25,
        nav: true,
        navText: ['', ''],
        dots: false,
        responsive: {
            768: {
                items: 2
            },
            1224: {
                items: 3
            }
        }
    });
});