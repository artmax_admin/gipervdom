//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/select2/dist/js/select2.min.js
//= ../../node_modules/owl.carousel/dist/owl.carousel.min.js
//= partials/carousels.js
//= partials/jquery.slimscroll.min.js
//= partials/jquery-ui.js
//= partials/jquery-ui.touch-punch.js
//= partials/jquery.maskedinput.js
//= partials/jquery.touchSwipe.min.js
// jQuery(window).on('load', function () {
//
// });
jQuery(document).ready(function ($) {
    let $preloader = $('.load-layer');
    $preloader.delay(350).fadeOut('slow');
  $('a').click(function (e) {
      if ($(this).attr('href') === '') {
          e.preventDefault();
      }
  });

  $.mask.definitions['~'] = '^[12345679]$';
  $.mask.definitions['*'] = '^[0123]$';
  $.mask.definitions['&'] = '^[01]$';
  $('input[type="tel"]').mask('+7 (~99) 999-9999');
  $('#personal_birthday, #date_delivery, #set_date, #set_dbrty').mask('*9/&9/9999');

  $('.active__section-title img').each(function(){
      var $img = $(this);
      var imgID = $img.attr('id');
      var imgClass = $img.attr('class');
      var imgURL = $img.attr('src');

      $.get(imgURL, function(data) {
          // Get the SVG tag, ignore the rest
          var $svg = $(data).find('svg');

          // Add replaced image's ID to the new SVG
          if(typeof imgID !== 'undefined') {
              $svg = $svg.attr('id', imgID);
          }
          // Add replaced image's classes to the new SVG
          if(typeof imgClass !== 'undefined') {
              $svg = $svg.attr('class', imgClass+' replaced-svg');
          }
          // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
          if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
              $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
          }

            // Replace image with new SVG
          $img.replaceWith($svg);
      },
      'xml');
  });
  $('select').not('.no-select2').select2({
      minimumResultsForSearch: -1
  });
  $('.send-title').on('click touchend', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      $(this).next().slideToggle('fast')
  });
  $('#show_filter').on('click touchend', function (e) {
      e.preventDefault();
      $('#filter_stores').addClass('animated');
      return false;
  });
  $('.select_location').on('click touchend', function (e) {
      e.preventDefault();
      $('#cities_select').addClass('animated');
      return false;
  });
  $('.city_select_trigger, #show_m_personal_menu, .js-select-city').on('click touchend', function (e) {
      e.preventDefault();
      $(this).next().toggleClass('animated');
      return false;
  });
  $('.js-show-cart').click(function (e) {
      e.preventDefault();
      $(this).addClass('active').next().toggleClass('animated');
      return false
  });
  $('#js-show-menu').on('click touchend', function (e) {
      e.preventDefault();
      $('.m_head-menu').addClass('animated');
      $('input').blur();
      return false;
  });

  $('#add_to_cart').click(function (e) {
      $('#added_to_cart').addClass('animated');
      return false;
  });

  $('.close, .close-link').on('click touchend', function (e) {
      e.preventDefault();
      $(this).parent().removeClass('animated');
  });

  $('#js-save_cart').click(function (e) {
      e.preventDefault();
      $('#save-list-note').addClass('animated');
      return false;
  });


  $('.co__subcategory').click(function (e) {
      e.preventDefault();
      $(this).addClass('active').siblings().removeClass('active');
      if (!$(this).attr('data-popular')) {
          $(this).parent().nextAll('.go-to-subcategory').css('visibility', 'visible')
      } else {
          $(this).parent().nextAll('.go-to-subcategory').css('visibility', 'hidden')
      }
  });

  $('.scrollable').slimScroll({
      height: '100%',
      size: '5px',
      color: '#C5C5C5'
  });

  $(window).scroll(function (e) {
      if ($(this).scrollTop() > 44) {
          $('.header_static').addClass('fixed')
      } else {
          $('.header_static').removeClass('fixed')
      }

      if ($(this).scrollTop() > $(this).height()) {
          $('#to_top').fadeIn('fast')
      } else {
          $('#to_top').fadeOut('fast')
      }
  });

  $('#js-edit-address').on('click touchend', function (e) {
      e.preventDefault();
      $('.popup-layer, .popup_select-location').fadeIn('fast');
  });

  $('.send-order-review').click(function (e) {
      e.preventDefault();
      $('.popup-layer, .send-review').fadeIn('fast');
  });

  $('.popup_first-visit .close, .popup_select-location .close, .js-button-ok, .send-review .close').on('click touchend', function (e) {
      e.preventDefault();
      $('.popup-layer, .popup').fadeOut('fast');
  });

  $('.popup-layer').click(function (e) {
      e.preventDefault();
      $('.popup-layer, .popup').fadeOut('fast');
  });

  $('.rating svg').hover(
      function () {
          $(this).addClass('rated').prevAll().addClass('rated');
          $(this).click(function (e) {
              $(this).addClass('rated-complete')
                  .prevAll().addClass('rated-complete')
                  .nextAll().removeClass('rated-complete');
          })
      },
      function () {
          $(this).removeClass('rated').prevAll().removeClass('rated')
      }
  );

  $('.filter-multi').click(function () {
      $(this).toggleClass('active');
  });

  $('#slider-range').slider({
      range: true,
      min: 0,
      max: 1000,
      values: [ 0, 1000 ],
      slide: function( event, ui ) {
          $( "#amount-min" ).val($( "#slider-range" ).slider( "values", 0 ));
          $('.min i').text($( "#slider-range" ).slider( "values", 0 ));
          $( "#amount-max" ).val($( "#slider-range" ).slider( "values", 1 ));
          $('.max i').text($( "#slider-range" ).slider( "values", 1 ));
      }
  });
  $( "#amount-min" ).val($( "#slider-range" ).slider( "values", 0 ));
  $( "#amount-max" ).val($( "#slider-range" ).slider( "values", 1 ));

  $('#show-filter').on('click touchend', function (e) {
      e.preventDefault();
      $('#js-show-filter').toggleClass('active')
  });

  $('#js-close-menu').on('click touchend', function (e) {
      e.preventDefault();
      $('.m_head-menu').removeClass('animated');
      return false;
  });

  $('.js-show-items').on('click touch', function (e) {
      e.preventDefault();
      $(this).toggleClass('active').next().slideToggle('fast').parent().toggleClass('active')
  });

    $('.confirm-promocode').on('click', function (e) {
        e.preventDefault();
        if ($(this).prev().val() !== '') {
            $('#confirm-promo').addClass('animated');
            $(this).prev().attr('disabled', true).val('');
        }
        return false;
    });

    $('#orderForm').submit(function (e) {
        // e.preventDefault();
        // $('input.required').each(function (e) {
        //     if($(this).val()==='') {
        //         $(this).addClass('invalid').parents('.order__block').addClass('invalid_block').find('.order__block-title')
        //             .append('<span class="form__error-message">Заполните обязательные поля</span>');
        //         $('html,body').animate({scrollTop: 0}, 600);
        //     } else {
        //         $('#confirm-offer').addClass('animated');
        //     }
        // })

        $('input.required').each(function () {
            if($(this).val()==='') {
                $(this).addClass('invalid')
                    .parents('.order__block').addClass('invalid_block')
                    .find('.form__error-message').text('Заполните обязательные поля');
                $('html,body').animate({scrollTop: 0}, 600);
                e.preventDefault();
            } else {
                $('#confirm-offer').addClass('animated');
            }
        });
    });

    $('#personal_data').submit(function (e) {
        let is_error = false;
        $('input.required').each(function () {
            if($(this).val()==='') {
                $(this).addClass('invalid')
                    .parents('.block-personal').addClass('invalid_block')
                    .find('.form__error-message').text('Заполните обязательные поля');
                $('html,body').animate({scrollTop: 0}, 600);
                is_error = true
            }
        });
        if(! $('input[name="conf"]').prop('checked') ) {
            $('input[name="conf"]').addClass('invalid');
            is_error = true;
        }
        if (is_error) {
            e.preventDefault();
        }
    });

    $('#log-in').submit(function (e) {
        let is_error = false;
        $(this).find('input.required').each(function () {
            if($(this).val()==='') {
                $(this).addClass('invalid');
                $('#log-in, #sign-in').addClass('invalid_data');
                is_error = true;
            }
        });
        if (is_error) {
            e.preventDefault();
        }
    });

    $('#sign-in').submit(function (e) {
        let is_error = false;
        $(this).find('input.required').each(function () {
            if($(this).val()==='') {
                $(this).addClass('invalid');
                $('#log-in, #sign-in').addClass('invalid_data');
                is_error = true;
            }
        });
        if(! $('input[name="sorm"]').prop('checked') ) {
            $('input[name="sorm"]').addClass('invalid');
            $(this).addClass('invalid_data');
            is_error = true;
        }
        if (is_error) {
            e.preventDefault();
        }
    });

    $('.m_user_form').submit(function(e) {
        let is_error = false;
        $('input.required').each(function () {
            if($(this).val()==='') {
                $(this).addClass('invalid');
                $('.m_user_form .card').addClass('invalid_data')
                    .find('.form__error-message').text('Заполните обязательные поля');
                $('html,body').animate({scrollTop: 0}, 600);
                is_error = true;
            }
        });
        if (is_error) {
            e.preventDefault();
        }

    });

    $('#personal_data-form').submit(function (e) {
        let is_error = false;
        $('input.required').each(function () {
           if ($(this).val() === '') {
               $(this).addClass('invalid')
                   .parents('.card').addClass('invalid_data')
                   .find('.form__error-message').text('Заполните обязательные поля');
               $('html,body').animate({scrollTop: 0}, 600);
               is_error = true;
           }
        });
        if (is_error) {
            e.preventDefault();
        }
    });

    $('.cart__offer').submit(function (e) {
        let is_error = false;
        $('input.required').each(function () {
            if ($(this).val() === '') {
                $(this).addClass('invalid')
                    .parents('.card').addClass('invalid_data')
                    .find('.form__error-message').text('Заполните обязательные поля');
                $('html,body').animate({scrollTop: 0}, 600);
                is_error = true
            }
        });
        if (!is_error) {
            $('.popup-layer, .popup_offered').fadeIn('fast');
            return false;
        } else {
            e.preventDefault();
        }
    });

    $('.login-popup form').submit(function (e) {
        let is_error = false;

        $('input.required').each(function () {
            if ($(this).val() === '') {
                $(this).addClass('invalid')
                    .parents('.login-popup').addClass('invalid_data')
                    .find('.form__error-message').text('Заполните обязательные поля');
                is_error = true;
            }
        });
        if (is_error) {
            e.preventDefault();
        }
    });

  $('.eq-minus, .eq-plus').click(function (e) {
      e.preventDefault();
  });

  $('#js-save_cart_m').on('click touchend', function (e) {
      e.preventDefault();
      $('.popup-layer, .popup_added').fadeIn('fast');
      return false;
  });

  $('#show_personal_menu, #show-sort').on('click touchend', function (e) {
      e.preventDefault();
      $(this).next().toggleClass('animated').stop();
      return false;
  });

  $(document).bind('click touchend', function(e) {
      var container = $('.animated');
      var multi_field = $('.filter-multi');
      if (!container.is(e.target) && container.has(e.target).length === 0) {
          container.removeClass('animated');
          $('.js-show-cart').removeClass('active');
          // return false;
      }
      if (!multi_field.is(e.target) && multi_field.has(e.target).length === 0) {
          multi_field.removeClass('active');
          // return false;
      }
  });

  $('#to_top').click(function(e) {
      e.preventDefault();
      $("html, body").animate({ scrollTop: 0 }, 600);
      return false;
  });

  $('.sort-by select, .show-by select').change(function(){
      $(this).width($(this).next().html($(this).find(':selected').html()).width());
  });

  $('.item__suggestions-block input[type="radio"]').change(function () {
      var parent = $(this).parents('.matching__content-block');
      if ($(this).hasClass('js-delete-item')) {
          parent.find('.current__item').addClass('is_deleted').append('<div class="item_delete"></div>');
      } else {
          parent.find('.current__item').removeClass('is_deleted').find('.item_delete').remove();
      }
  });

  $('.catalog__element, .catalog_element').click(function (e) {
      if (e.target.className !== 'element_info') {
          $(this).find('span').attr('style', 'display: flex;');
          var element_added = '<div class="added" style="' +
              'position:fixed;' +
              'left:'+(e.currentTarget.getBoundingClientRect().left - 10)+'px;' +
              'top:'+(e.currentTarget.getBoundingClientRect().top - 10)+'px;' +
              'width: 200px;' +
              'height: 347px;"></div>';
          $('body').append(element_added);
          $('.added').addClass('active').animate({
              'top': 0,
              'left': $(window).width(),
              'opacity': 0,
              'width': 50,
              'height': 50
          }, 600);
          setTimeout(function () {
              $('.added').remove();
          }, 600);
          ++e.currentTarget.querySelector('span').innerHTML;
          e.preventDefault();
      }
  });
  $('#add_to_cart-button').click(function (e) {
      var element_added = '<div class="added" style="' +
          'position:fixed;' +
          'left:'+ document.getElementById('item-image').getBoundingClientRect().left +'px;' +
          'top:'+ document.getElementById('item-image').getBoundingClientRect().top +'px;' +
          'width:' +document.getElementById('item-image').getBoundingClientRect().width + 'px;' +
          'height:' +document.getElementById('item-image').getBoundingClientRect().height + 'px"></div>';
      $('body').append(element_added);
      $('.added').addClass('active').animate({
          'top': 0,
          'left': $(window).width(),
          'opacity': 0,
          'width': 50,
          'height': 50
      }, 600);
      setTimeout(function () {
          $('.added').remove();
      }, 600);
      e.preventDefault();
  });
    $('#set_date, #set_dbrty, #personal_birthday').datepicker({
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
            'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        firstDay: 1,
        dateFormat: 'dd/mm/yy'
    });
    $('.show-photo').click(function (e) {
        e.preventDefault();
        $('body').append('<div class="popup_image"><img src="'+$(this).attr('href')+'"</div>');
        $('.popup_image').animate({
            'display': 'flex',
            'opacity': '1'
        }, 200).click(function (e) {
            $(this).fadeOut(200).remove()
        });
    });
    $('.m_head-menu').swipe({
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            if (direction === "left") {
                $(this).removeClass('animated');
            }
        },
        threshold: 75
    });
});
document.addEventListener('click', function (e) {
    if (e.target.classList.contains("eq-plus")) {
        ++e.target.parentElement.querySelector("input").value;
    } else if (e.target.classList.contains("eq-minus")) {
        --e.target.parentElement.querySelector("input").value;
        if (e.target.parentElement.querySelector("input").value < 0) {
            e.target.parentElement.querySelector("input").value = 0;
        }
    }
});